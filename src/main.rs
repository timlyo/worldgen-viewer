extern crate kiss3d;
extern crate nalgebra as na;
extern crate worldgen;

use na::{Vector3, UnitQuaternion};
use kiss3d::window::Window;
use kiss3d::light::Light;
use worldgen::world::World;

use worldgen::world::elevation::Elevation;
use na::geometry::Translation3;

fn main() {
    let mut world = World::new(500.0, 500.0);

    world.elevation.insert(Elevation{x:0.0, y: 0.0, elevation: 50.0});
    world.elevation.insert(Elevation{x:50.0, y: 0.0, elevation: 30.0});
    world.elevation.insert(Elevation{x:0.0, y: 50.0, elevation: 30.0});
    world.elevation.insert(Elevation{x:50.0, y: 50.0, elevation: 30.0});

    let mut window = Window::new("Kiss3d: cube");
    let mut c      = window.add_group();
    for point in world.elevation.iter() {
        window.add_sphere(1.0)
            .set_local_translation(Translation3::new(point.x as f32, point.y as f32, point.elevation as f32));
    }

    c.set_color(1.0, 0.0, 0.0);

    window.set_light(Light::StickToCamera);

    let rot = UnitQuaternion::from_axis_angle(&Vector3::y_axis(), 0.014);

    while window.render() {
        c.prepend_to_local_rotation(&rot);
    }
}